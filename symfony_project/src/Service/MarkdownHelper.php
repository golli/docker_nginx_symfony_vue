<?php

namespace App\Service;

use Knp\Bundle\MarkdownBundle\MarkdownParserInterface;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\Cache\CacheInterface;
use function Symfony\Component\String\s;

class MarkdownHelper
{
    /**
     * @var CacheInterface
     */
    private $cache;
    /**
     * @var MarkdownParserInterface
     */
    private $parser;
    /**
     * @var bool
     */
    private $isDebug;
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(CacheInterface $cache, MarkdownParserInterface $parser,bool $isDebug, LoggerInterface $mdLogger)
    {
        $this->cache= $cache;
        $this->parser=$parser;
        $this->isDebug=$isDebug;

        $this->logger = $mdLogger;
    }

    public function parse(string $source):string
    {
        if (stripos($source,'cat') !== false){
            $this->logger->info('Meo!');
        }
        if ($this->isDebug){
            return $this->parser->transformMarkdown($source);
        }
        return $this->cache->get('markdown_'.md5($source),function () use ($source){
            return $this->parser->transformMarkdown($source);
        });
    }

}